# Task

Install Front end app to consume flask apis through Kong api gateway.
## Installation

I used 3 instances ECS cluster and create a service for each task definition and three ELB one for each tier (Frontend,Kong,FlaskApi) and RDS instance.
 
![img](https://wahba-bucket.s3.amazonaws.com/ar.jpg)

## Usage

To consume flask api in development environment
```bash
export app="Frontend_URL_ELB"
#To display main page
curl http://${app}/api/dev/
#To list users
curl http://${app}/api/dev/users
#To add user
curl -X POST -H "Content-Type: application/json" --url http://${app}/api/dev/add \
                        --data '{"name": "mohammad",
                                  "email": "myemail@wahba.com",
                                  "pwd": "12345"}'
#To delete user
curl -X DELETE --url http://${app}/api/dev/delete/{user_ID}
#To show specific user 
curl  --url http://${app}/api/dev/user/{user_ID}

```
To consume flask api in development environment
```bash
export app="Frontend_URL_ELB"
#To display main page
curl http://${app}/api/prod/
#To list users
curl http://${app}/api/prod/users
#To add user
curl -X POST -H "Content-Type: application/json" --url http://${app}/api/prod/add \
                        --data '{"name": "mohammad",
                                  "email": "myemail@wahba.com",
                                  "pwd": "12345"}'
#To delete user
curl -X DELETE --url http://${app}/api/prod/delete/{user_ID}
#To show specific user 
curl  --url http://${app}/api/prod/user/{user_ID}


