# Frontend App

It as a basic nginx container that serves basic web page and consumes two apis one for development and the other for production.


Required environment variable:
```bash
API_HOST="xxxx"
```
## Usage

```bash
cd nginx
docker build -t frontend-app .
docker run -d -p 80:80  -e API_HOST="xxx" frontend-app
 
```
Running container image on ECS is [Here](https://cloud.docker.com/u/muwahba/repository/docker/muwahba/frontend-app)
