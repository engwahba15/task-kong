# API Deployment

bash script to deploy apis to ECS
## Installation

Make sure you configure aws with access key and secret access key and region
Run ecs-deploy.sh first after customizing it with env variables and then run kong-deplou.sh.


```bash
aws configure
./ecs-deploy.sh
./kong-deploy.sh
```

This will configure kong but you still have to modify frontend configuration files to forward to kong service.
