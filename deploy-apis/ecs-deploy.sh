#!/bin/bash


CLUSTER_NAME="xxxxxxx"
SERVICE_NAME="xxxx"
TASK_NAME="xxxx"
COUNT="xxx"

#get current version if there is any and add 1 to deploy new revision
CURRENT_REVISION=`aws ecs describe-task-definition --task-definition ${TASK_NAME} | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//'`


if [ $CURRENT_REVISION -gt 1 ] ;then
 let "REVISION=$CURRENT_REVISION+1"
else
 let REVISION=1
fi

#create task definition for API
aws ecs register-task-definition  --cli-input-json file://node-TD.json
#Create a service from that task taskDefinition

aws ecs create-service \
    --cluster $CLUSTER_NAME\
    --service-name $SERVICE_NAME \
    --task-definition $TASK_NAME:$REVISION \
    --desired-count $COUNT

