#!/bin/bash

export kong="xxxx"
export api="xxxxx"
export port="xxxx"
export svc_name="xxx"
export paths="xxx"


#create kong service
curl -i -X POST \
 --url http://${kong}:8001/services/ \
 --data "name=${svc_name}" \
 --data "url=http://${api}:${port}"

#create routes
curl -i -X POST \
 --url http://${kong}:8001/services/${svc_name}/routes \
 --data "paths[]=${paths}" \
 --data 'strip_path=true'

